import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { SimpleSmoothScrollService } from 'ng2-simple-smooth-scroll';
import { SimpleSmoothScrollOption } from 'ng2-simple-smooth-scroll';

import { faRobot } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faShareSquare } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  faRobot = faRobot;
  faEnvelope = faEnvelope;
  faShareSquare = faShareSquare;

  constructor(private smooth: SimpleSmoothScrollService) { }

  customOptions: OwlOptions = {
    loop: true,
    center: true,
    autoplay: true,
    autoplayTimeout: 3000,
    margin: 20,
    nav: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1000: {
        items: 3
      }
    }
  }

  ngOnInit(): void {
    this.smooth.smoothScrollToAnchor();
  }

  goTop() {
    this.smooth.smoothScrollToTop({ duration: 1000, easing: 'linear' });
  }

}
